sudo apt-get update
sudo apt-get install vim aptitude htop
sudo apt-get install curl net-tools cron
sudo apt-get install tshark
sudo usermod -aG wireshark robot
sudo adduser $USER dialout
sudo systemctl set-default multi-user.target
